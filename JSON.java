import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class JSON {
	@SuppressWarnings("unchecked")
	public static void main(String[] args)  throws IOException {
		Scanner sc=new Scanner(System.in);
		System.out.println("THESE ARE FILE IO OPERATIONS");
		System.out.println("----------------------------");
		System.out.println("1.CREATE A NEW FILE");
		System.out.println("2.APPEND DATA TO THE NEW FILE");
		System.out.println("3.UPDATE NEW DATE TO THE FILE");
		System.out.println("4.DELETE THE DATA FROM THE FILE");
		System.out.println("5.DELETE THE FILE");
		System.out.println("6.Enter your choice");
		int ch=sc.nextInt();
		switch(ch) {
		case 1:
			//create a new file
		try
		{
		    File file = new File("C:\\Users\\swgaddam\\newworkspace\\Fileio\\src\\createfile.json");
		    file.createNewFile();
		    FileWriter filewriter=new FileWriter( file);
		    System.out.println("file");
		    filewriter.write("swapna");
		    filewriter.close();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace(); 
		}
		break;
		case 2:
			//write date to the file
			File file=new File("C:\\Users\\swgaddam\\newworkspace\\Fileio\\src\\createfile.json");  
			FileWriter fileWriter = new FileWriter(file);
			JSONObject Userobject = new JSONObject(); 
			Userobject.put("Name", "Swapna"); 
			Userobject.put("age", "23");
			Userobject.put("gender", "female");
			Userobject.put("designation", "IT employee");
			Userobject.put("location", "hyderbad");
			JSONArray listOfhobbies = new JSONArray(); 
			listOfhobbies.add("singing");
			listOfhobbies.add("eating");
			Userobject.put("hobby", listOfhobbies);
			fileWriter.write(Userobject.toJSONString());  
            fileWriter.flush();  
            fileWriter.close();  
            System.out.println("file data added");
			break;
		case 3:
			//update data to the file
	}
		sc.close();
}
}
